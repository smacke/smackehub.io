Readme for the risk solver branch.

In order to pull in changes directly to personal website,
switch to the risk-solver branch, which is linked to this
repo. Then, do a pull, create a temp branch, and rebase
that branch onto master (of smacke.github.io).
